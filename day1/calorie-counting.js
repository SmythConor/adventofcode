const utils = require('../utils/utils')

async function getData() {
	const input = (await utils.readFile('./day1/input')).map(i => parseInt(i))

	return input.reduce((prev, acc) => {
		if (isNaN(acc)) {
			prev.elfCount++;
			return prev;
		} else {
			const prevCount = prev.calorieCounts[prev.elfCount] || 0;
			prev.calorieCounts[prev.elfCount] = acc + prevCount;

			return prev;
		}
	}, { elfCount: 0, calorieCounts: [] })
}

async function part1() {
	const data = await getData();

	const highest = Math.max(...data.calorieCounts);

	console.log(highest);
}

async function part2() {
	const data = await getData();

	data.calorieCounts.sort((a, b) => b - a)

	const topThreeCount = data.calorieCounts[0] + data.calorieCounts[1] + data.calorieCounts[2];

	console.log(topThreeCount);
}

part1()
part2()