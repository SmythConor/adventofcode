const { readFile } = require('../utils/utils');
const mathjs = require("mathjs");

async function getData() {
	const input = (await readFile('./day11/input')).map(l => l.trim());

	const monkeys = [];
	let monkeyTracker = 0;

	for (let i = 0; i < input.length; i++) {
		const line = input[i];

		// Monkey 0:
		if (line.indexOf('Monkey') === 0) {
			const monkeyNum = Number(/Monkey ([0-9])*:/.exec(line)[1]);
			monkeys.push({ num: monkeyNum });
			monkeyTracker = monkeyNum;
		}

		// Starting items: 79, 98
		if (line.indexOf('Starting items') === 0) {
			const monkey = monkeys[monkeyTracker];
			monkey.items = line.split(':')[1].trim().split(',').map(v => Number(v));
		}

		// Operation: new = old * 19
		if (line.indexOf('Operation') === 0) {
			const monkey = monkeys[monkeyTracker];
			const splitLine = line.split(':')[1].trim();
			const regex = /^new = old ([+\-*/]) (old|[0-9]*)$/.exec(splitLine);
			const operation = {
				op: regex[1],
				ex: regex[2]
			};
			monkey.operation = operation;
		}

		// Test: divisible by 13
		if (line.indexOf('Test') === 0) {
			const monkey = monkeys[monkeyTracker];
			monkey.test = Number(/divisible by ([0-9]*)/.exec(line.split(':')[1].trim())[1]);
		}

		// If true: throw to monkey 1
		// If false: throw to monkey 3
		if (line.indexOf('If') === 0) {
			const monkey = monkeys[monkeyTracker];
			const regex = /If (true|false): throw to monkey ([0-9]*)/.exec(line);
			const is = `is${regex[1] === 'true' ? true : false}`
			monkey[is] = Number(regex[2]);
		}
	}

	return monkeys;
}

async function run(monkeys, rounds, reliever) {
	const activityCounter = [];
	for (let i = 0; i < monkeys.length; i++) {
		activityCounter[i] = 0;
	}

	for (let i = 0; i < rounds; i++) {
		for (let monkeyTracker = 0; monkeyTracker < monkeys.length; monkeyTracker++) {
			const monkey = monkeys[monkeyTracker];
			const operation = monkey.operation;

			for (let itemTracker = 0; itemTracker < monkey.items.length; itemTracker++) {
				const item = monkey.items[itemTracker];
				activityCounter[monkeyTracker]++;

				const worryLevel = doOperation(operation.op, item, operation.ex);
				const relievedWorryLevel = reliever(worryLevel);
				const test = (() => {
					const testResult = relievedWorryLevel % monkey.test;
					if (testResult) {
						return 'false'
					}

					return 'true';
				})();
				const throwsTo = monkey[`is${test}`];

				// console.log('monkey:', monkeyTracker);
				// console.log('worry:', worryLevel);
				// console.log('relief:', relievedWorryLevel);
				// console.log('throw:', throwsTo);
				monkeys[throwsTo].items.push(relievedWorryLevel);
			}

			monkey.items = [];
		}
	}

	const topTwo = activityCounter.sort((a, b) => b - a).slice(0, 2);

	const combinedActivity = topTwo[0] * topTwo[1];

	console.log(combinedActivity);
}

async function part1() {
	const monkeys = await getData();
	run(monkeys, 20, (worryLevel) => Math.floor(worryLevel / 3));
}

function getAllMonkeyTests(monkeys) {
	return monkeys.flatMap(monkey => monkey.test);
}

async function part2() {
	const monkeys = await getData();
	const testLcm = mathjs.lcm(...getAllMonkeyTests(monkeys));
	run(monkeys, 10000, (worryLevel) => worryLevel % testLcm);
}

part1();

part2();

const operations = {
	'+': (old, x) => old + x,
	'-': (old, x) => old - x,
	'*': (old, x) => old * x,
	'/': (old, x) => old / x
}

function doOperation(operation, item, x) {
	if (x === 'old') {
		return operations[operation](item, item);
	}

	return operations[operation](item, Number(x));
}