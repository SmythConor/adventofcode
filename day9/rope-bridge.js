const { readFile, sum } = require('../utils/utils');

async function getData() {
	return (await readFile('day9/test-input-2'))
		.map(line => {
			const comps = line.split(' ');
			return {
				direction: comps[0],
				steps: Number(comps[1])
			};
		});
}
const GRID_SIZE = 1000;

function generateGrid(input) {

	const rows = []

	for (let i = 0; i < GRID_SIZE; i++) {
		const col = [];

		for (let j = 0; j < GRID_SIZE; j++) {
			col.push(false);
		}

		rows.push(col);
	}

	return rows;
}

function adjustTail(hX, hY, tX, tY, maxDistance) {
	const dist = Math.abs(tX - hX) + Math.abs(tY - hY);
	const md = maxDistance + 1;

	if (dist > maxDistance) {
		const diffX = hX - tX;
		const diffY = hY - tY;

		if (diffX === md) {
			if (diffY >= 1) { // right and down
				return { hX, hY, tX: tX + 1, tY: tY + 1 };
			}
			if (diffY <= -1) { // right and up
				return { hX, hY, tX: tX + 1, tY: tY - 1 };
			}

			return { hX, hY, tX: tX + 1, tY };
		}

		if (diffX === -md) {
			if (diffY >= 1) { // left and down
				return { hX, hY, tX: tX - 1, tY: tY + 1 };
			}
			if (diffY <= -1) { // left and up
				return { hX, hY, tX: tX - 1, tY: tY - 1 };
			}

			return { hX, hY, tX: tX - 1, tY };
		}

		if (diffY === md) {
			if (diffX >= 1) { // down and right
				return { hX, hY, tX: tX + 1, tY: tY + 1 };
			}
			if (diffX <= -1) { // down and left
				return { hX, hY, tX: tX - 1, tY: tY + 1 };
			}

			return { hX, hY, tX, tY: tY + 1 };
		}

		if (diffY === -md) {
			if (diffX >= 1) { // up and right
				return { hX, hY, tX: tX + 1, tY: tY - 1 };
			}
			if (diffX <= -1) { // up and left
				return { hX, hY, tX: tX - 1, tY: tY - 1 };
			}

			return { hX, hY, tX, tY: tY - 1 };
		}
	}

	return { hX, hY, tX, tY };
}

async function part1() {
	const instructions = (await getData());

	const grid = generateGrid();

	let sY = GRID_SIZE / 2;
	let sX = GRID_SIZE / 2;

	// indicate starting position ss true
	grid[sY][sX] = true;

	let hY = sY;
	let hX = sX;

	let tY = sY;
	let tX = sX;

	for (let i = 0; i < instructions.length; i++) {
		const { direction, steps } = instructions[i];

		const directionFunction = directionFunctions[direction];

		for (let j = 0; j < steps; j++) {
			({ hX, hY } = directionFunction(hX, hY, tX, tY));
			({ hX, hY, tX, tY } = adjustTail(hX, hY, tX, tY, 1));
			grid[tY][tX] = true;
			// console.log(direction, j);
			// printGrid(grid);
		}
	}

	// printGrid(grid);

	const result = grid.flatMap(v => v).filter(v => v).reduce(sum, 0);

	console.log('actual:', result, 'expected: 6384');
}

async function part2() {
	const instructions = (await getData());

	const grid = generateGrid();

	let sY = GRID_SIZE / 2;
	let sX = GRID_SIZE / 2;

	// indicate starting position ss true
	grid[sY][sX] = true;

	let hY = sY;
	let hX = sX;

	let tY = sY;
	let tX = sX;

	for (let i = 0; i < instructions.length; i++) {
		const { direction, steps } = instructions[i];

		const directionFunction = directionFunctions[direction];

		for (let j = 0; j < steps; j++) {
			({ hX, hY } = directionFunction(hX, hY, tX, tY));
			({ hX, hY, tX, tY } = adjustTail(hX, hY, tX, tY, 9));
			grid[tY][tX] = true;
			console.log(direction, j);
			printGrid(grid);
		}
	}

	const result = grid.flatMap(v => v).filter(v => v).reduce(sum, 0);

	console.log('actual:', result, 'expected: ???');
}

// part1();

part2();

const directionFunctions = {
	'R': function (hX, hY) {
		return {
			hX: hX + 1,
			hY
		};
	},
	'L': (hX, hY) => {
		return {
			hX: hX - 1,
			hY
		};
	},
	'U': (hX, hY) => {
		return {
			hX,
			hY: hY - 1,
		};
	},
	'D': (hX, hY) => {
		return {
			hX,
			hY: hY + 1,
		};
	}
}

function findFirst(grid) {
	let startingRow = 0;
	for (let i = startingRow; i < grid.length; i++) {
		const row = grid[i];
		const o = row.findIndex(v => v);
		if (o >= 0) {
			return o;
		}
	}
}

function findLast(grid) {
	let startingRow = 0;
	for (let i = startingRow; i < grid.length; i++) {
		const row = grid[i];
		const o = row.findLastIndex(v => v);
		if (o >= 0) {
			return o;
		}
	}
}

function findF(grid) {
	for (let i = 0; i < grid.length; i++) {
		if (grid[i].includes(true)) {
			return i;
		}
	}
}

function findL(grid) {
	for (let i = grid.length - 1; i >= 0; i--) {
		if (grid[i].includes(true)) {
			return i;
		}
	}
}

function printGrid(grid) {
	const start = findF(grid);
	const end = findL(grid) + 1;
	const s = findFirst(grid);
	const e = findLast(grid) + 1;

	let rowOutput = '';
	for (let i = start; i < end; i++) {
		const row = grid[i];
		for (let j = s; j < e; j++) {
			const col = grid[i][j];
			if (col) {
				rowOutput += '#'
			} else {
				rowOutput += '.'
			}
		}
		rowOutput += '\n'
	}

	console.log(rowOutput);
}