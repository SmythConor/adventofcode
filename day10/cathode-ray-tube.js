const { readFile, sum } = require('../utils/utils');

async function getData() {
	const input = (await readFile('./day10/input'))
		.map(line => {
			const splitLine = line.split(' ');
			const instruction = splitLine[0];
			if (splitLine.length > 1) {
				return {
					instruction,
					num: Number(splitLine[1])
				}
			}

			return { instruction };
		})

	return input;
}

async function part1() {
	const input = (await getData());

	let cycleCounter = 0;
	let cycleValues = [];
	let register = 1;
	const relevantCycleCount = [20, 60, 100, 140, 180, 220];

	for (let i = 0; i < input.length; i++) {
		const line = input[i];

		if (line.instruction === 'noop') {
			cycleCounter++;

			if (relevantCycleCount.includes(cycleCounter)) {
				cycleValues.push(register * cycleCounter);
			}

			continue;
		}

		cycleCounter++;

		if (relevantCycleCount.includes(cycleCounter)) {
			cycleValues.push(register * cycleCounter)
		}

		cycleCounter++;

		if (relevantCycleCount.includes(cycleCounter)) {
			cycleValues.push(register * cycleCounter)
		}

		register += line.num;
	}

	console.log(cycleValues);
	const result = cycleValues.reduce(sum, 0);
	console.log(result);
}

function generateScreen() {
	const screen = [];

	for (let i = 0; i < 6; i++) {
		const row = [];

		for (let j = 0; j < 40; j++) {
			row.push('.');
		}

		screen.push(row);
	}
	console.log(screen.length)
	console.log(screen[0].length)

	return screen;
}

function drawScreen(screen, spritePosition, cycleCounter) {
	const row = Math.floor(cycleCounter / 40);
	const internalCycleCounter = (cycleCounter % 40) - 1;

	if (internalCycleCounter <= spritePosition + 1 && internalCycleCounter >= spritePosition - 1) {
		screen[row][internalCycleCounter] = '#'
	}
	// else if (internalCycleCounter === spritePosition) {
	// 	screen[row][internalCycleCounter] = '#'
	// } else if (internalCycleCounter === spritePosition - 1) {
	// 	screen[row][internalCycleCounter] = '#'
	// }
}

function printScreen(screen) {
	let output = '';
	for (let i = 0; i < screen.length; i++) {
		const row = screen[i];
		for (let j = 0; j < row.length; j++) {
			output += row[j];
		}
		output += '\n';
	}

	console.log(output);
}

async function part2() {
	const input = (await getData());
	const screen = generateScreen();

	let cycleCounter = 0;
	let cycleValues = [];
	let spritePosition = 1;

	for (let i = 0; i < input.length; i++) {
		const line = input[i];

		printScreen(screen);

		if (line.instruction === 'noop') {
			cycleCounter++;
			drawScreen(screen, spritePosition, cycleCounter);
			continue;
		}

		cycleCounter++;
		drawScreen(screen, spritePosition, cycleCounter);

		cycleCounter++;
		drawScreen(screen, spritePosition, cycleCounter);

		spritePosition += line.num;
	}

	printScreen(screen);

	console.log(cycleValues);
	const result = cycleValues.reduce(sum, 0);
	console.log(result);
}

// part1();
part2();