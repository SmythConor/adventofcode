const utils = require('../utils/utils');

async function getData() {
	const input = (await utils.readFile('./day5/input'))
	const stackDiagramEnds = input.indexOf('');
	const stackDiagram = input.slice(0, stackDiagramEnds - 1).map(row => {
		const splitRow = row.split('')
		const output = [];

		for (let i = 0; i < splitRow.length - 2; i += 4) {
			const s = splitRow.slice(i, i + 3);
			output.push(s[1].trim());
		}

		return output;
	});

	const stackList = input
		.slice(stackDiagramEnds - 1, stackDiagramEnds)[0]
		.split(/( *[0-9] *)/g).map(col => col.trim()).filter(col => col)

	const stacks = stackList
		.reduce((stacks, stackCounter) => {
			stacks[stackCounter] = [];
			return stacks;
		}, {});

	stackDiagram.reduceRight((prev, row) => {
		for (let i = 0; i < row.length; i++) {
			const data = row[i]
			if (data) {
				prev[i + 1].push(data)
			}
		}

		return prev;
	}, stacks)

	const instructions = input.slice(stackDiagramEnds + 1, input.length)
		.map((instruction) => {
			const groups = /move ([0-9]*) from ([0-9]) to ([0-9])/.exec(instruction);

			return {
				count: groups[1],
				from: groups[2],
				to: groups[3]
			}
		});

	return {
		instructions,
		stacks
	}
}

function moveOne(count, from, to) {
	for (let i = 0; i < count; i++) {
		const removed = from.pop();
		to.push(removed);
	}
}

function moveStack(count, from, to) {
	const removed = [];
	for (let i = 0; i < count; i++) {
		removed.push(from.pop());
	}
	const len = removed.length;
	for (let i = 0; i < len; i++) {
		to.push(removed.pop());
	}
}

async function doInstructions(moveFn) {
	const { stacks, instructions } = await getData();

	instructions.forEach(instruction => {
		const { count, from, to } = instruction;
		moveFn(count, stacks[from], stacks[to]);
	});

	const output = Object.keys(stacks).reduce((output, key) => {
		const stack = stacks[key];
		return output + stack[stack.length - 1];
	}, '');

	console.log(output);
}

function part1() {
	doInstructions(moveOne);
}

function part2() {
	doInstructions(moveStack);
}

part1()
part2();