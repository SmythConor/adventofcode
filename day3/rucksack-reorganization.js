const utils = require('../utils/utils')

function generatePriorities() {
	let lowerStart = 'a'.charCodeAt(0)
	let lowerEnd = 'z'.charCodeAt(0);

	let upperStart = 'A'.charCodeAt(0);
	let upperEnd = 'Z'.charCodeAt(0);

	let output = {}
	let ranking = 1;

	for (let i = lowerStart; i <= lowerEnd; i++, ranking++) {
		const c = String.fromCharCode(i);

		output[c] = ranking;
	}

	for (let i = upperStart; i <= upperEnd; i++, ranking++) {
		const c = String.fromCharCode(i);

		output[c] = ranking;
	}

	return output;
}

async function getData() {
	const input = (await utils.readFile('./day3/input'))

	return input;
}

function innerLoop(comp1, comp2, prios) {
	for (let j = 0; j < comp1.length; j++) {
		const c = comp1[j];
		const io = comp2.indexOf(c)

		if (io >= 0) {
			const cc = comp2.charAt(io)
			const prio = prios[cc];
			return prio;
		}
	}

	return 0;
}

async function part1() {
	const data = (await getData())
		.map(line => {
			const compSize = line.length / 2;

			if (line.length % 2 != 0) console.log(compSize);

			const comp1 = line.substring(0, compSize);
			const comp2 = line.substring(compSize, line.length)

			const output = { comp1, comp2 }

			if (comp1.length !== comp2.length) console.log(output);

			return output;
		});

	const prios = generatePriorities();

	const tracker = [];

	for (let i = 0; i < data.length; i++) {
		const iter = data[i];
		let found = false;

		tracker.push(innerLoop(iter.comp1, iter.comp2, prios));
	}

	const result = tracker.reduce((prev, acc) => prev + acc, 0);
	console.log(result);
}

function sortByArrLength(ar1, ar2, ar3) {
	const ars = [ar1, ar2, ar3];
	ars.sort((a, b) => a.length - b.length)

	return {
		comp1: ars[0],
		comp2: ars[1],
		comp3: ars[2]
	}
}

async function part2() {
	const data = (await getData());
	const groups = [];
	const prios = generatePriorities();

	for (let i = 0; i < data.length; i += 3) {
		const { comp1, comp2, comp3 } = sortByArrLength(data[i], data[i + 1], data[i + 2])
		groups.push({ comp1, comp2, comp3 })
	}

	const result = groups.map(({ comp1, comp2, comp3 }) => {
		for (let i = 0; i < comp1.length; i++) {
			const c = comp1.charAt(i);

			if (comp2.indexOf(c) >= 0 && comp3.indexOf(c) >= 0) {
				return prios[c]
			}
		}

		return 0;
	}).reduce((prev, acc) => prev + acc, 0);

	console.log(result);
}


part1();

part2();