const utils = require('../utils/utils');

function sortSmallest(ars) {
	ars.sort((a, b) => {
		const aL = a.finish - a.start;
		const bL = b.finish - b.start;

		return aL - bL
	});

	return {
		comp1: ars[0],
		comp2: ars[1]
	};
}

async function getData() {
	const input = (await utils.readFile('./day4/input'))
		.map(line => line.split(','))
		.map(line => {
			const p1 = line[0].split('-');
			const p2 = line[1].split('-');

			return [{
				start: Number(p1[0]),
				finish: Number(p1[1]),
				str: line[0]
			}, {
				start: Number(p2[0]),
				finish: Number(p2[1]),
				str: line[1]
			}]
		})
		.map(line => {
			return sortSmallest(line);
		});

	return input;
}

async function part1() {
	const input = (await getData())
		.reduce((prev, line) => {
			const a = line.comp1;
			const b = line.comp2;

			if (a.start >= b.start && a.finish <= b.finish) return prev + 1;

			return prev;
		}, 0)

	console.log(input);
}

async function part2() {
	const input = (await getData())
		.reduce((prev, line) => {
			const a = line.comp1;
			const b = line.comp2;

			if (a.start >= b.start && a.start <= b.finish) return prev + 1;
			if (a.finish >= b.start && a.finish <= b.finish) return prev + 1;

			return prev;
		}, 0)

	console.log(input);
}

part1();

part2();