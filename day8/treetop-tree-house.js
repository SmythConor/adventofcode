const { readFile } = require('../utils/utils');

async function getData() {
	const input = (await readFile('./day8/input')).map(line => line.split('')).map(line => line.map(l => Number(l)));

	return input;
}

function searchLeft(grid, y, x) {
	const value = grid[y][x];

	for (let i = x - 1; i >= 0; i--) {
		const comp = grid[y][i];

		if (comp >= value) {
			return false;
		}
	}

	return true;
}

function searchRight(grid, y, x) {
	const value = grid[y][x];

	for (let i = x + 1; i < grid[y].length; i++) {
		const comp = grid[y][i];

		if (comp >= value) {
			return false;
		}
	}

	return true;
}

function searchUp(grid, y, x) {
	const value = grid[y][x];

	for (let i = y - 1; i >= 0; i--) {
		const comp = grid[i][x];

		if (comp >= value) {
			return false;
		}
	}

	return true;
}

function searchDown(grid, y, x) {
	const value = grid[y][x];

	for (let i = y + 1; i < grid.length; i++) {
		const comp = grid[i][x];

		if (comp >= value) {
			return false;
		}
	}

	return true;
}

function isVisible(grid, y, x) {
	return searchLeft(...arguments)
		|| searchRight(...arguments)
		|| searchUp(...arguments)
		|| searchDown(...arguments);
}

async function part1() {
	const data = await getData();

	let counter = 0;

	for (let i = 0; i < data.length; i++) {
		for (let j = 0; j < data[i].length; j++) {
			if (isVisible(data, i, j)) counter++;
		}
	}

	console.log(counter);
}

function scenicLeft(grid, y, x) {
	const value = grid[y][x];

	let score = 0;

	for (let i = x - 1; i >= 0; i--) {
		const comp = grid[y][i];

		score++;

		if (comp >= value) return score;
	}

	return score;
}

function scenicRight(grid, y, x) {
	const value = grid[y][x];

	let score = 0;

	for (let i = x + 1; i < grid[y].length; i++) {
		const comp = grid[y][i];

		score++;

		if (comp >= value) return score;
	}

	return score;
}

function scenicUp(grid, y, x) {
	const value = grid[y][x];

	let score = 0;

	for (let i = y - 1; i >= 0; i--) {
		const comp = grid[i][x];

		score++;

		if (comp >= value) return score;
	}

	return score;
}

function scenicDown(grid, y, x) {
	const value = grid[y][x];

	let score = 0;

	for (let i = y + 1; i < grid.length; i++) {
		const comp = grid[i][x];

		score++;

		if (comp >= value) return score;
	}

	return score;
}

function calcScenic(grid, y, x) {
	return scenicUp(...arguments)
		* scenicDown(...arguments)
		* scenicLeft(...arguments)
		* scenicRight(...arguments);
}

async function part2() {
	const data = await getData();

	let max = 0;

	for (let i = 0; i < data.length; i++) {
		for (let j = 0; j < data[i].length; j++) {
			const score = calcScenic(data, i, j);
			if (score > max) max = score;
		}
	}

	console.log(max);
}

part1();

part2();