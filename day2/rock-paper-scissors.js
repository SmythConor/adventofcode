const utils = require('../utils/utils')

const KEY_MAP = {
	'A': 'ROCK',
	'B': 'PAPER',
	'C': 'SCISSORS',
	'X': 'ROCK',
	'Y': 'PAPER',
	'Z': 'SCISSORS'
}

const KEY_MAP_WIN_LOSE = {
	'X': 'LOSS',
	'Y': 'DRAW',
	'Z': 'WIN'
}

const RANKS = {
	'ROCK': 1,
	'PAPER': 2,
	'SCISSORS': 3
}
// us - them = result mine us
// 1 - 2 = -1 loss
// 1 - 3 = -2 win
// 2 - 1 = 1 win
// 2 - 3 = -1 loss
// 3 - 1 = 2 loss
// 3 - 2 = 1 win

const DESIGNATION_POINTS = {
	'WIN': 6,
	'DRAW': 3,
	'LOSS': 0
}

const RESULT_MAP = {
	'ROCK': {
		WIN: 'SCISSORS',
		LOSS: 'PAPER'
	},
	'PAPER': {
		WIN: 'ROCK',
		LOSS: 'SCISSORS'
	},
	'SCISSORS': {
		WIN: 'PAPER',
		LOSS: 'ROCK'
	}
}

function getDesignationPoints(elf, us) {
	const result = us - elf;

	if (result === 0) {
		return DESIGNATION_POINTS['DRAW']
	}

	if (result === -1 || result === 2) {
		return DESIGNATION_POINTS['LOSS']
	}

	return DESIGNATION_POINTS['WIN']
}

function decryptLine(input) {
	const selected = RANKS[input.mine];

	const result = selected - RANKS[input.elf];

	if (result === 0) {
		return DESIGNATION_POINTS['DRAW'] + selected;
	}

	if (result === -1 || result === 2) {
		return DESIGNATION_POINTS['LOSS'] + selected;
	}

	return DESIGNATION_POINTS['WIN'] + selected;
}

function decryptLine1(line) {
	const outcome = KEY_MAP_WIN_LOSE[line.result];
	const points = DESIGNATION_POINTS[outcome]
	const selected = KEY_MAP[line.vs];

	if (outcome === 'DRAW') {
		return points + RANKS[line.elf];
	}

	const s = RESULT_MAP[selected];

	const whatweselected = s[outcome === 'WIN' ? 'LOSS' : 'WIN'];

	return points + RANKS[whatweselected];
}

async function getData() {
	return (await utils.readFile('./day2/input'))
		.map(line => {
			const inputs = line.split(' ')
			return {
				elf: KEY_MAP[inputs[0]],
				mine: KEY_MAP[inputs[1]],
				result: inputs[1],
				vs: inputs[0]
			}
		})

}

async function part1() {
	const result = (await getData())
		.map(line => decryptLine(line))
		.reduce((prev, acc) => prev + acc, 0)

	console.log(result); // 8890
}

async function part2() {
	const result = (await getData())
		.map(line => decryptLine1(line))
		.reduce((prev, acc) => prev + acc, 0)

	console.log(result); //10238
}

part1();
part2();