const utils = require('../utils/utils');

async function getData() {
	const input = (await utils.readFile('./day6/input'))

	return input[0].split('');
}

function hasDuplicates(arr) {
	for (let i = 0; i < arr.length; i++) {
		const c1 = arr[i];

		for (let j = i + 1; j < arr.length; j++) {
			const c2 = arr[j];

			if (c1 === c2) {
				return true;
			}
		}
	}
}

function findBySize(input, size) {
	for (let i = 0; i < input.length; i++) {
		const arr = input.slice(i, i + size);
		const isMessage = !hasDuplicates(arr);

		if (isMessage) {
			return i + size;
		}
	}
}

async function part1() {
	const input = (await getData());

	const result = findBySize(input, 4);
	console.log(result);
}

async function part2() {
	const input = (await getData());

	const result = findBySize(input, 14);
	console.log(result);
}

part1();

part2();