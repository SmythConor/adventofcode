const { readFile, sum } = require('../utils/utils');

const COMMAND_TYPES = {
	CHANGE_DIRCTORY: 'cd',
	LIST_FILES: 'ls'
}

async function getData() {
	return await readFile('./day7/input');
}

function file(filename, filesize) {
	this.name = filename;
	this.size = filesize;
}

file.prototype.getSize = function () {
	return this.size;
}

file.prototype.isDirectory = () => false;
file.prototype.isFile = () => true

function dir(dirName, dirParent) {
	this.name = dirName;
	this.files = [];
	this.parent = dirParent;
	this.size = 0;
}

dir.prototype.addFile = function (file) {
	this.files.push(file);
}

dir.prototype.getDirectory = function (name) {
	return this.files.find((file) => file.isDirectory() && file.name === name);
}

dir.prototype.getParent = function () {
	return this.parent;
}

dir.prototype.getFiles = function () {
	return this.files.filter(file => file.isFile());
}

dir.prototype.getDirectories = function () {
	return this.files.filter(file => file.isDirectory());
}

dir.prototype.getSize = function () {
	if (this.size === 0) {
		const fileCount = this.getFiles().map(file => file.getSize()).reduce(sum, 0)
		const dirCount = this.getDirectories().map(dir => dir.getSize()).reduce(sum, 0);
		this.size = fileCount + dirCount;
	}

	return this.size;
}

dir.prototype.isDirectory = () => true
dir.prototype.isFile = () => false

const isCommand = (command) => command.indexOf('$') === 0;
const isDirectory = (command) => command.indexOf('dir') === 0;

function parseCommand(command) {
	const splitCommand = command.split(' ');
	const commandType = splitCommand[1];

	if (commandType === COMMAND_TYPES.CHANGE_DIRCTORY) {
		const targetDirectory = splitCommand[2];

		return {
			commandType,
			targetDirectory
		}
	} else if (commandType === COMMAND_TYPES.LIST_FILES) {
		return {
			commandType
		}
	}
}

async function buildDirectoryTree() {
	const commands = (await getData());

	let context = new dir('/', '/');
	const direcoryTree = context;

	// Just assume that the first command will be 'cd /'
	for (let i = 1; i < commands.length; i++) {
		const command = commands[i];

		if (isCommand(command)) {
			const parsedCommand = parseCommand(command);

			if (parsedCommand.commandType === COMMAND_TYPES.LIST_FILES) {
				let counter = 0;
				let j = i + 1;
				do {
					counter++;
					const nextInput = commands[j];
					const spl = nextInput.split(' ');

					if (isDirectory(nextInput)) {
						const d = new dir(spl[1], context);
						context.addFile(d)
					} else {
						const f = new file(spl[1], Number(spl[0]));
						context.addFile(f);
					}
					j++;
				} while (j < commands.length && !isCommand(commands[j]))

				i += counter;
			} else if (parsedCommand.commandType === COMMAND_TYPES.CHANGE_DIRCTORY) {
				if (parsedCommand.targetDirectory === '..') {
					context = context.getParent();
				} else {
					context = context.getDirectory(parsedCommand.targetDirectory);
				}
			}
		}
	}

	return direcoryTree;
}

function flattenDriectories(directoryTree) {
	const directories = directoryTree.getDirectories();

	if (directories.length === 0) {
		return directories;
	}

	const subDirectories = directories.flatMap(directory => flattenDriectories(directory));

	return [...directories, ...subDirectories]
}

async function part1() {
	const direcoryTree = await buildDirectoryTree();
	const directories = flattenDriectories(direcoryTree);

	const count = directories
		.map(directory => directory.getSize())
		.filter(size => {
			return size <= 100000
		})
		.reduce(sum, 0);

	console.log(count);
}

async function part2() {
	const TOTAL_SIZE = 70000000;
	const SIZE_NEEDED = 30000000;

	const direcoryTree = await buildDirectoryTree();
	const directories = flattenDriectories(direcoryTree);

	const currentSize = direcoryTree.getSize()
	const minSpaceNeeded = SIZE_NEEDED - (TOTAL_SIZE - currentSize);

	const count = Math.min(...directories
		.map(directory => directory.getSize())
		.filter(directorySize => directorySize >= minSpaceNeeded))

	console.log(count);
}

part1();
part2();